function cadastrarConta(enderecoEmailCadC, tipoCadC, fotoCadC){
    var tokenrecuperado = JSON.parse(localStorage.getItem("token"));
    var obj = {
            endereco : enderecoEmailCadC,
            tipo: tipoCadC,
            foto: fotoCadC,
            token : tokenrecuperado.token
        }
        sessionStorage.setItem("foto", fotoCadC);
        var json = JSON.stringify(obj);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", 'http://www.henriquesantos.pro.br:8080/api/email/contas/new', true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                console.log(xhr.responseText);
                mostrarModal("Conta cadastrada")
            }else{
                mostrarModal("Conta não pode ser cadastrada")

            }
        }
        xhr.send(json);     
       
}

function listarContasUsuario(){
    var tokenrecuperado = JSON.parse(localStorage.getItem("token"));
    var xhr = new XMLHttpRequest();
    xhr.open("GET", 'http://www.henriquesantos.pro.br:8080/api/email/contas/'+tokenrecuperado.token, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var contaslista = JSON.parse(xhr.responseText);
            contaslista.forEach(function(conta){
                contas.innerHTML +="<a href='mail.html' class='list-group-item' onclick='recuperarContaEspecifica("+conta.id+")'>"+conta.endereco+"</a>";
            });
        }else{
            mostrarModal("Conta não pode ser cadastrada")

        }
    }

    xhr.send();     
}

function recuperarContaEspecifica(conta_id){
    var tokenrecuperado = JSON.parse(localStorage.getItem("token"));
    localStorage.setItem("contaSelecionada", conta_id)
    var xhr = new XMLHttpRequest();
    xhr.open("GET", 'http://www.henriquesantos.pro.br:8080/api/email/contas/'+tokenrecuperado.token+'/conta/'+conta_id, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
    
           sessionStorage.setItem("emailSelecionado", xhr.responseText);
           location.href = "mail.html"            
        }else{
            
        }
    }
    xhr.send();
}